var express = require('express');
var router = express.Router();

/* GET cms */
router.get('/', function(req, res, next) {
  res.redirect('/cms/dashboard');
});

router.get('/dashboard', function(req, res, next) {
  res.render('cms/dashboard', { title: 'Dashboard', class:'dashboard', subclass:'dashboard1' });
});

router.get('/dashboard2', function(req, res, next) {
  res.render('cms/dashboard2', { title: 'Dashboard2', class:'dashboard', subclass:'dashboard2' });
});

router.get('/widgets', function(req, res, next) {
  res.render('cms/pages/widget', { title: 'Widget', class:'widget', subclass:'widget' });
});

router.get('/boxed', function(req, res, next) {
  res.render('cms/pages/layout/boxed', { title: 'Layout Boxed', class:'layout', subclass:'boxed' });
});

router.get('/collapsed-sidebar', function(req, res, next) {
  res.render('cms/pages/layout/collapse-sidebar', { title: 'Collapsed Sidebar', class:'layout', subclass:'collapsed-sidebar' });
});

router.get('/fixed', function(req, res, next) {
  res.render('cms/pages/layout/fixed', { title: 'Fixed', class:'layout', subclass:'fixed' });
});

router.get('/top-nav', function(req, res, next) {
  res.render('cms/pages/layout/top-nav', { title: 'Top Nav', class:'layout', subclass:'top-nav' });
});

router.get('/chartjs', function(req, res, next) {
  res.render('cms/pages/charts/chartjs', { title: 'Chart Js', class:'charts', subclass:'chartjs' });
});

router.get('/flot', function(req, res, next) {
  res.render('cms/pages/charts/flot', { title: 'flot', class:'charts', subclass:'flot' });
});

router.get('/inline', function(req, res, next) {
  res.render('cms/pages/charts/inline', { title: 'inline', class:'charts', subclass:'inline' });
});

router.get('/morris', function(req, res, next) {
  res.render('cms/pages/charts/morris', { title: 'Morris Js', class:'charts', subclass:'morris' });
});

module.exports = router;
